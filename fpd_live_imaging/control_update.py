import time
from threading import Thread


class ControlUpdate(object):

    def __init__(self, acquisition_control):
        self.acquisition_control = acquisition_control
        self._update_looper = True

    def start_update_thread(self):
        self.update_thread = Thread(target=self._update_method)
        self.update_thread.start()

    def stop_running(self):
        self._update_looper = False
        time.sleep(0.2)

    def _update_method(self):
        ac = self.acquisition_control
        cpara = ac._icomm.control_parameters
        scan_control = ac._icomm.scan_control
        while self._update_looper:
            if cpara['minus_scan_pos']:
                ac.minus_one_pixel_to_scan_postition()
                cpara['minus_scan_pos'] = False
            if cpara['plus_scan_pos']:
                ac.plus_one_pixel_to_scan_postition()
                cpara['plus_scan_pos'] = False
            if cpara['restart_scan']:
                ac.restart_scan()
                cpara['restart_scan'] = False
            if cpara['set_scan_size']:
                ac.resize_scan(scan_control['x'], scan_control['y'])
                cpara['set_scan_size'] = False
            if cpara['add_bf']:
                ac.start_bf_process()
                cpara['add_bf'] = False
            if cpara['add_adf']:
                ac.start_adf_process()
                cpara['add_adf'] = False
            if cpara['add_comxt']:
                ac.start_com_x_threshold_process()
                cpara['add_comxt'] = False
            if cpara['add_comyt']:
                ac.start_com_y_threshold_process()
                cpara['add_comyt'] = False
            if cpara['add_diffraction']:
                ac.start_full_diffraction_image_process()
                cpara['add_diffraction'] = False
            if cpara['add_diffraction_fft']:
                ac.start_full_image_FFT_process()
                cpara['add_diffraction_fft'] = False
            if cpara['add_diffraction_threshold']:
                ac.start_full_diffraction_thresholded_image_process()
                cpara['add_diffraction_threshold'] = False
            if cpara['stop_running']:
                ac.shutdown_everything()
                self.stop_running()
                cpara['stop_running'] = False
            if cpara['start_medipix_receiver']:
                ac.start_medipix_receiver()
                cpara['start_medipix_receiver'] = False
            if cpara['insert_detector']:
                if hasattr(ac, 'detector_control'):
                    ac.detector_control.insert()
                else:
                    print("Hardware detector control not initialized")
                cpara['insert_detector'] = False
            if cpara['retract_detector']:
                if hasattr(ac, 'detector_control'):
                    ac.detector_control.retract()
                else:
                    print("Hardware detector control not initialized")
                cpara['retract_detector'] = False
            time.sleep(0.1)
