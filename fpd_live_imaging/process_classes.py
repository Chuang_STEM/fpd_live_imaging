import numpy as np
import multiprocessing as mp
from scipy.ndimage.measurements import center_of_mass
from scipy.optimize import leastsq
import time


class DataProcessingBase(object):
    def __init__(self, name, output_queue, process_variables=[]):
        self.name = name
        self.receive_queue = mp.Queue()
        self.output_queue = output_queue
        self.process_variables = process_variables
        self.base_level_correction = mp.Value('f', 0.)
        self.fraction_correction = mp.Value('f', 1.)
        self.processes_active = mp.Value('b', True)

        self.process_sleep_time = 1.e-7

    def __repr__(self):
        return '<%s %s (B:%s,C:%s) (I:%s)>' % (
                self.__class__.__name__,
                self.name,
                self.base_level_correction.value,
                self.fraction_correction.value,
                self.receive_queue.qsize())

    def stop_running(self):
        self.processes_active.value = False

    def clear_queues(self):
        while not self.receive_queue.empty():
            self.receive_queue.queue.get()
        while not self.output_queue.empty():
            self.output_queue.queue.get()

    def start_process_function(self):
        self.process_process = mp.Process(target=self._data_parsing_process)
        self.process_process.start()

    def _data_parsing_process(self):
        while self.processes_active.value:
            time.sleep(self.process_sleep_time)
            if not self.receive_queue.empty():
                diffraction_image = self.receive_queue.get()
                pixel_value = self.process_function(
                                diffraction_image)
                pixel_value -= self.base_level_correction.value
                pixel_value /= self.fraction_correction.value
                self.output_queue.put(int(pixel_value))
                del diffraction_image, pixel_value

    def _make_circular_mask(
            self, centerX, centerY,
            imageSizeX, imageSizeY, radius):
        y, x = np.ogrid[
                -centerX:imageSizeX-centerX,
                -centerY:imageSizeY-centerY]
        mask = x*x + y*y <= radius*radius
        return(mask)

    def _make_adf_mask(
            self, centerX, centerY,
            imageSizeX, imageSizeY, radius0, radius1):
        inner_mask = self._make_circular_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius0)
        outer_mask = self._make_circular_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius1)
        return(np.logical_xor(inner_mask, outer_mask))

    def _make_segment_bool_array(
            self, centerX, centerY,
            angle0, angle1, imageSizeX, imageSizeY):
        angle0 = angle0 % (2*np.pi)
        angle1 = angle1 % (2*np.pi)
        y, x = np.ogrid[
            -centerX:imageSizeX - centerX,
            -centerY:imageSizeY - centerY]
        t = np.arctan2(x, y)+np.pi
        if angle0 < angle1:
            bool_array = (t > angle0) * (t < angle1)
        else:
            bool_array = (t > angle0) + (t < angle1)
        return(bool_array)

    def _make_segment_mask(
            self, centerX, centerY, imageSizeX, imageSizeY,
            radius0, radius1, angle0, angle1):
        inner_mask = self._make_circular_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius0)
        outer_mask = self._make_circular_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius1)
        segment_array = self._make_segment_bool_array(
                centerX, centerY, angle0, angle1, imageSizeX, imageSizeY)
        mask = np.logical_xor(inner_mask, outer_mask)
        mask *= segment_array
        return(mask)

    def _make_bf_mask(
            self, centerX=128, centerY=128,
            imageSizeX=256, imageSizeY=256, radius=20):
        self.bf_mask = self._make_circular_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius)


class FullDiffractionImageProcess(DataProcessingBase):

    def _data_parsing_process(self):
        while self.processes_active.value:
            time.sleep(self.process_sleep_time)
            if not self.receive_queue.empty():
                diffraction_image = self.receive_queue.get()
                self.output_queue.put(diffraction_image)
                del diffraction_image


class FullDiffractionImageFFTProcess(DataProcessingBase):
    """Process for displaying FFT of image data

    Examples
    --------
    >>> import fpd_live_imaging.testing_tools as tt
    >>> import fpd_live_imaging.acquisition_control_class as acc
    >>> from fpd_live_imaging.test_images.test_images import linux_penguin_64
    >>> ac = acc.LiveImagingQt()
    >>> test_detector = tt.TestDetectorInputImage(
    ...     number_of_frames=64*64, input_image=linux_penguin_64)
    >>> test_detector.sleep_time.value = 0.001
    >>> test_detector.start_data_listening()
    >>> ac._comm_medi.port = test_detector.port
    >>> proc_name = ac.start_full_image_FFT_process()
    >>> rd = ac.process_dict[proc_name]['proc']
    >>> rd.base_level_correction.value = 0
    >>> rd.fraction_correction.value = 0.1

    >>> ac.start_medipix_receiver()

    After the image has finished

    >>> ac.cancel_all_processes()
    >>> test_detector.stop_send_process()

    >>> ac.shutdown_everything()

    """

    def _data_parsing_process(self):
        while self.processes_active.value:
            time.sleep(self.process_sleep_time)
            if not self.receive_queue.empty():
                diffraction_image = self.receive_queue.get()
                fft_im = np.fft.fft2(diffraction_image)
                fft_im = np.fft.fftshift(fft_im)
                fft_im = np.log(np.abs(fft_im))
                self.output_queue.put(fft_im)
                del diffraction_image


class DiffractionImageRollingProcess(DataProcessingBase):
    """Process for getting rolling average diffraction data.

    Attributes
    ----------
    frames_to_average : int, optional
        Number of frames to average.
    rolling_mode : string, optional, default 'flat'
        Options are 'flat' for uniform weighting and 'exp' for
        exponential weighting.

    Examples
    --------
    >>> import fpd_live_imaging.testing_tools as tt
    >>> import fpd_live_imaging.acquisition_control_class as acc
    >>> from fpd_live_imaging.test_images.test_images import linux_penguin_64
    >>> ac = acc.LiveImagingQt()
    >>> test_detector = tt.TestDetectorInputImage(
    ...     number_of_frames=64*64, input_image=linux_penguin_64)
    >>> test_detector.sleep_time.value = 0.001
    >>> test_detector.start_data_listening()
    >>> ac._comm_medi.port = test_detector.port
    >>> proc_name = ac.start_full_diffraction_rolling_image_process()
    >>> rd = ac.process_dict[proc_name]['proc']
    >>> rd.frames_to_average = 19
    >>> rd.rolling_mode = 'exp'

    After the image has finished

    >>> ac.cancel_all_processes()
    >>> test_detector.stop_send_process()

    """

    def __init__(self, *args, **kwds):
        rm = kwds.pop('rolling_mode', None)
        super(DiffractionImageRollingProcess, self).__init__(
                *args, **kwds)
        self._frames_to_average = mp.Value('i', 10)
        self._framesize_x = mp.Value('i', 256)
        self._framesize_y = mp.Value('i', 256)
        self._refresh_frame_array = mp.Value('b', True)

        self._manager = mp.Manager()
        self._rolling_modes = self._manager.dict()
        self._rolling_modes.update({'flat': 0,
                                    'exp': 1})
        self._rolling_mode = mp.Value('i', 0)
        self.rolling_mode = rm

    @property
    def frames_to_average(self):
        return self._frames_to_average.value

    @frames_to_average.setter
    def frames_to_average(self, value):
        self._frames_to_average.value = value
        self._refresh_frame_array.value = True

    @property
    def rolling_mode(self):
        rm_keys = list(self._rolling_modes.keys())
        rm_vals = list(self._rolling_modes.values())
        return rm_keys[rm_vals.index(self._rolling_mode.value)]

    @rolling_mode.setter
    def rolling_mode(self, value):
        default_rm = 'flat'
        if value is None:
            rm = self._rolling_modes[default_rm]
        elif value not in self._rolling_modes.keys():
            print("'rolling_mode' key %s not valid. Using default: %s" % (
                value, default_rm))
            print("Valid keys:", self._rolling_modes.keys())
            rm = self._rolling_modes[default_rm]
        else:
            rm = self._rolling_modes[value]
        self._rolling_mode.value = rm
        self._refresh_frame_array.value = True

    def reset(self):
        self._refresh_frame_array.value = True

    def _data_parsing_process(self):
        index = 0
        frame_shape = (self._framesize_x.value, self._framesize_y.value)
        while self.processes_active.value:
            if self._refresh_frame_array.value:
                image_array = np.zeros(
                        shape=(
                            self._frames_to_average.value,
                            frame_shape[0], frame_shape[1]))
                self._refresh_frame_array.value = False
                index = 0
                self._incomplete_array = True

            time.sleep(self.process_sleep_time)
            if not self.receive_queue.empty():
                diffraction_image = self.receive_queue.get()
                image_array[index, :, :] = diffraction_image
                if self._rolling_mode.value == 0:
                    if self._incomplete_array:
                        mean_image = np.sum(image_array, axis=0)/(index+1.0)
                    else:
                        mean_image = np.mean(image_array, axis=0)
                elif self._rolling_mode.value == 1:
                    mean_image = self._rolling_mode_calculation(
                            image_array, index)
                self.output_queue.put(mean_image)
                del diffraction_image, mean_image
                index += 1
                if index > (self._frames_to_average.value-1):
                    index = 0
                    self._incomplete_array = False

    def _rolling_mode_calculation(self, image_array, index):
        i = -np.arange(self._frames_to_average.value-1, -1, -1)
        i = np.roll(i, index+1)
        wti = np.exp(i/self._frames_to_average.value)
        temp_array = image_array*wti[:, None, None]
        if self._incomplete_array:
            mean_image = (
                    temp_array[:index+1].sum(axis=0))/(wti.cumsum()[index])
        else:
            mean_image = temp_array.sum(axis=0) / wti.sum()
        return mean_image


class FullDiffractionThresholdedImageProcess(DataProcessingBase):
    def __init__(self, *args, **kwds):
        super(FullDiffractionThresholdedImageProcess, self).__init__(
                *args, **kwds)
        self.threshold_multiplier = mp.Value('f', 1.)

    def _data_parsing_process(self):
        while self.processes_active.value:
            time.sleep(self.process_sleep_time)
            if not self.receive_queue.empty():
                diffraction_image = self.receive_queue.get()
                threshold = np.mean(
                        diffraction_image)*self.threshold_multiplier.value
                diffraction_image[diffraction_image < threshold] = 0
                diffraction_image[diffraction_image >= threshold] = 1
                self.output_queue.put(diffraction_image)
                del diffraction_image


class BFDetectorProcess(DataProcessingBase):
    def process_function(self, diffraction_image):
        pixel_value = diffraction_image.sum(dtype=np.uint32)
        return(pixel_value)


class ADFDetectorProcess(DataProcessingBase):
    """Process for getting annular dark field data.

    Attributes
    ----------
    centreX, centreY : float
    radius0 : float
        Inner radius of the annular virtual detector.
    radius1 : float
        Outer radius of the annular virtual detector.

    Examples
    --------
    >>> import fpd_live_imaging.testing_tools as tt
    >>> import fpd_live_imaging.acquisition_control_class as acc
    >>> from fpd_live_imaging.test_images.test_images import linux_penguin_64
    >>> acquisition_control = acc.LiveImagingQt()
    >>> test_detector = tt.TestDetectorInputImage(
    ...     number_of_frames=64*64, input_image=linux_penguin_64)
    >>> test_detector.sleep_time.value = 0.001
    >>> test_detector.start_data_listening()
    >>> acquisition_control._comm_medi.port = test_detector.port
    >>> proc_name = acquisition_control.start_adf_process()
    >>> adf0 = acquisition_control.process_dict[proc_name]['proc']
    >>> adf0.centreX = 140
    >>> adf0.centreY = 110
    >>> adf0.radius0 = 10
    >>> adf0.radius1 = 20

    After the image has finished

    >>> acquisition_control.cancel_all_processes()
    >>> test_detector.stop_send_process()

    """
    def __init__(self, *args, **kwds):
        super(ADFDetectorProcess, self).__init__(*args, **kwds)
        self._centreX = mp.Value('f', 128.)
        self._centreY = mp.Value('f', 128.)
        self._radius0 = mp.Value('f', 10.)
        self._radius1 = mp.Value('f', 20.)
        self._refresh_mask = mp.Value('b', True)
        self.annotation_array = None

    def process_function(self, diffraction_image):
        if self._refresh_mask.value:
            self.generate_adf_detector(
                    self._centreX.value, self._centreY.value,
                    self._radius0.value, self._radius1.value)
        masked_diffraction_image = diffraction_image
        masked_diffraction_image[self.adf_mask] = 0
        pixel_value = masked_diffraction_image.sum(dtype=np.uint32)
        return(pixel_value)

    @property
    def centreX(self):
        return self._centreX.value

    @centreX.setter
    def centreX(self, value):
        self._centreX.value = value
        self.annotation_array[0] = value
        self._refresh_mask.value = True

    @property
    def centreY(self):
        return self._centreY.value

    @centreY.setter
    def centreY(self, value):
        self._centreY.value = value
        self.annotation_array[1] = value
        self._refresh_mask.value = True

    @property
    def radius0(self):
        return self._radius0.value

    @radius0.setter
    def radius0(self, value):
        self._radius0.value = value
        self.annotation_array[2] = value
        self._refresh_mask.value = True

    @property
    def radius1(self):
        return self._radius1.value

    @radius1.setter
    def radius1(self, value):
        self._radius1.value = value
        self.annotation_array[3] = value
        self._refresh_mask.value = True

    def generate_adf_detector(
            self, centerX, centerY, radius0, radius1,
            imageSizeX=256, imageSizeY=256):
        self.adf_mask = self._make_adf_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius0, radius1)
        self._refresh_mask.value = False


class SegmentedProcess(DataProcessingBase):
    def __init__(self, *args, **kwds):
        super(SegmentedProcess, self).__init__(*args, **kwds)
        self._centreX = mp.Value('f', 128.)
        self._centreY = mp.Value('f', 128.)
        self._radius0 = mp.Value('f', 10.)
        self._radius1 = mp.Value('f', 20.)
        self._angle = mp.Value('f', 0.)
        self._refresh_mask = mp.Value('b', True)
        self.annotation_array = None

    def process_function(self, diffraction_image):
        if self._refresh_mask.value:
            self.generate_segmented_detector(
                self._centreX.value, self._centreY.value,
                self._radius0.value, self._radius1.value,
                self._angle.value)
        sum0 = diffraction_image[self.mask0].sum(dtype=np.int64)
        sum1 = diffraction_image[self.mask1].sum(dtype=np.int64)
        pixel_value = np.uint32((sum0 - sum1 + 0.5*(2**32)))
        return(pixel_value)

    @property
    def centreX(self):
        return self._centreX.value

    @centreX.setter
    def centreX(self, value):
        self._centreX.value = value
        self.annotation_array[0] = value
        self._refresh_mask.value = True

    @property
    def centreY(self):
        return self._centreY.value

    @centreY.setter
    def centreY(self, value):
        self._centreY.value = value
        self.annotation_array[1] = value
        self._refresh_mask.value = True

    @property
    def radius0(self):
        return self._radius0.value

    @radius0.setter
    def radius0(self, value):
        self._radius0.value = value
        self.annotation_array[2] = value
        self._refresh_mask.value = True

    @property
    def radius1(self):
        return self._radius1.value

    @radius1.setter
    def radius1(self, value):
        self._radius1.value = value
        self.annotation_array[3] = value
        self._refresh_mask.value = True

    @property
    def angle(self):
        return self._angle.value

    @angle.setter
    def angle(self, value):
        self._angle.value = value
        self._refresh_mask.value = True

    def generate_segmented_detector(
            self, centreX, centreY, radius0, radius1,
            angle, imageSizeX=256, imageSizeY=256):
        self.mask0 = self._make_segment_mask(
                centreX, centreY, imageSizeX, imageSizeY,
                radius0, radius1, angle, angle+(np.pi/2))
        self.mask1 = self._make_segment_mask(
                centreX, centreY, imageSizeX, imageSizeY,
                radius0, radius1, angle+np.pi, angle+(np.pi/2)+np.pi)
        self._refresh_mask.value = False


class SinglePixelProcess(DataProcessingBase):
    def process_function(self, diffraction_image):
        x, y = self.process_variables[0]
        pixel_value = diffraction_image[x, y]
        return(pixel_value)


class CoMxProcess(DataProcessingBase):
    def process_function(self, diffraction_image):
        CoMx, CoMy = center_of_mass(diffraction_image)
        return(CoMx)


class CoMyProcess(DataProcessingBase):
    def process_function(self, diffraction_image):
        CoMx, CoMy = center_of_mass(diffraction_image)
        return(CoMy)


class CoMxThresholdProcess(DataProcessingBase):
    def __init__(self, *args, **kwds):
        super(CoMxThresholdProcess, self).__init__(*args, **kwds)
        self.threshold_multiplier = mp.Value('f', 1.)

    def process_function(self, diffraction_image):
        threshold = np.mean(diffraction_image)*self.threshold_multiplier.value
        diffraction_image[diffraction_image < threshold] = 0
        diffraction_image[diffraction_image >= threshold] = 1
        CoMx, CoMy = center_of_mass(diffraction_image)
        return(CoMx)


class CoMyThresholdProcess(DataProcessingBase):
    def __init__(self, *args, **kwds):
        super(CoMyThresholdProcess, self).__init__(*args, **kwds)
        self.threshold_multiplier = mp.Value('f', 1.)

    def process_function(self, diffraction_image):
        threshold = np.mean(diffraction_image)*self.threshold_multiplier.value
        diffraction_image[diffraction_image < threshold] = 0
        diffraction_image[diffraction_image >= threshold] = 1
        CoMx, CoMy = center_of_mass(diffraction_image)
        return(CoMy)


class CoMxMaskDiskProcess(DataProcessingBase):
    def process_function(self, diffraction_image):
        diffraction_image[self.bf_mask] = 0.0
        CoMx, CoMy = center_of_mass(diffraction_image)
        return(CoMx)


class CoMyMaskDiskProcess(DataProcessingBase):
    def process_function(self, diffraction_image):
        diffraction_image[self.bf_mask] = 0.0
        CoMx, CoMy = center_of_mass(diffraction_image)
        return(CoMy)


class HolzProcessing(DataProcessingBase):
    # This is completely untested, will probably not run on first try
    def process_function(self, diffraction_image):
        annulus_sum0 = (self.annulus0*diffraction_image).sum(dtype=np.uint32)
        annulus_sum1 = (self.annulus1*diffraction_image).sum(dtype=np.uint32)
        annulus_sum2 = (self.annulus2*diffraction_image).sum(dtype=np.uint32)
        annulus_sum3 = (self.annulus3*diffraction_image).sum(dtype=np.uint32)

        xdata = np.array([self.radius0, self.radius1, self.radius2])
        ydata = np.array([annulus_sum0, annulus_sum1, annulus_sum2])

        qout, success = leastsq(
                self.errfunc, [max(ydata), -1, -0.5],
                args=(xdata, ydata), maxfev=3000)

        background_value = self._fitfunc(
                [qout[0], qout[1], qout[2]],
                self.integrate_radius3)
        pixel_value = annulus_sum3 - background_value
        return(pixel_value)

    def _fitfunc(self, p, x):
        return p[0] + p[1] * (x ** p[2])

    def _errfunc(self, p, x, y):
        return y - self.fitfunc(p, x)

    def generate_annular_masks(
            self, centerX, centerY,
            imageSizeX, imageSizeY,
            radius00, radius01, radius10, radius11,
            radius20, radius21, radius30, radius31):
        self.radius0 = (radius01+radius00)*0.5
        self.radius1 = (radius11+radius10)*0.5
        self.radius2 = (radius21+radius20)*0.5
        self.integrate_radius3 = (radius31+radius30)*0.5
        self.annulus0 = self._make_adf_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius00, radius01)
        self.annulus1 = self._make_adf_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius10, radius11)
        self.annulus2 = self._make_adf_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius20, radius21)
        self.integrate_annulus3 = self._make_adf_mask(
                centerX, centerY, imageSizeX, imageSizeY, radius30, radius31)


class PhaseCorrelationProcess(DataProcessingBase):
    """Process for getting a phase correlation DPC component.

    Note: this class requires the fpd library.

    Attributes
    ----------
    ref_image : None or 2-D array, optional
        Reference image of shape (256, 256). The type will be cast to
        float. If not set, the first image acquired will be used as the
        reference. Set to None for the next acquired image to be taken
        as the new reference.
    min_images_to_process : int, optional
        The minimum number of images to acquire before processing them.
    max_update_seconds : float, optional
        Maximum time in which to acquire images before processing.
    component_angle : float, optional
        Angle of the DPC signal to be returned, in degrees. The default
        is the x-component.
    proc_func_args : dictionary
        Phase correlation function arguments. For details, see
        fpd.fpd_processing.phase_correlation.

    Notes
    -----
    The data is buffered and then multiple images are processed at once
    for efficiency. The calculations are triggered by the first of
    `min_images_to_process` or `max_update_seconds` to be exceeded.

    The default parameters are suitable for disks within the centre
    128x128 pixels.

    Examples
    --------
    >>> import fpd_live_imaging.testing_tools as tt
    >>> import fpd_live_imaging.acquisition_control_class as acc
    >>> scan_len = 32
    >>> test_detector = tt.TestDiskImages(
    ...     scan_len=scan_len,
    ...     radius=32, nscans=2, fast_but_big=True) # doctest: +SKIP
    >>> test_detector.sleep_time.value = 0.001 # doctest: +SKIP
    >>> test_detector.start_data_listening() # doctest: +SKIP
    >>> ac = acc.LiveImagingQt()
    >>> ac._comm_medi.port = test_detector.port # doctest: +SKIP
    >>> proc_name = ac.start_phase_correlation_process() # doctest: +SKIP
    >>> rd = ac.process_dict[proc_name]['proc'] # doctest: +SKIP
    >>> rd.base_level_correction.value = -3 # doctest: +SKIP
    >>> rd.fraction_correction.value = 0.01 # doctest: +SKIP
    >>> rd.min_images_to_process.value = 256 # doctest: +SKIP
    >>> rd.proc_func_args['crop_r'] = 64 # doctest: +SKIP
    >>> rd.proc_func_args['parallel'] = True # doctest: +SKIP
    >>> rd.proc_func_args['ncores'] = 4 # doctest: +SKIP
    >>> rd.max_update_seconds.value = 0.5 # doctest: +SKIP
    >>> rd.component_angle.value = 90.0 # doctest: +SKIP
    >>> rd.ref_image = None # doctest: +SKIP
    >>> ac.resize_scan(*(scan_len,)*2)
    >>> ac.start_medipix_receiver()

    After the images have finished

    >>> ac.cancel_all_processes()
    >>> test_detector.stop_send_process() # doctest: +SKIP

    """

    def __init__(self, *args, **kwds):
        super(PhaseCorrelationProcess, self).__init__(
                *args, **kwds)

        try:
            from fpd.fpd_processing import phase_correlation
            self._im_proc_func = phase_correlation
        except ImportError:
            raise Exception(
                    'Phase correlation module cannot be loaded from fpd.')

        self._ref_image = mp.Array('d', 256*256, lock=True)
        self._ref_image_set = False
        self.min_images_to_process = mp.Value('i', 128)  # 16 MB in 12bit mode
        self.max_update_seconds = mp.Value('f', 0.5)
        self.component_angle = mp.Value('f', 0.0)

        self._manager = mp.Manager()
        self.proc_func_args = self._manager.dict()
        self.proc_func_args.update({'nr': None,
                                    'nc': None,
                                    'cyx': (128,)*2,
                                    'crop_r': 64,
                                    'sigma': 2.0,
                                    'spf': 20,
                                    'pre_func': None,
                                    'post_func': None,
                                    'mode': '1d',
                                    'rebin': 4,
                                    'truncate': 4.0,
                                    'parallel': False,
                                    'ncores': 1,
                                    'print_stats': False})
        self._image_buffer = []  # holds images before processing
        self._previous_time = None
        self._time_now = None

    @property
    def ref_image(self):
        return self._ref_image.value

    @ref_image.setter
    def ref_image(self, value):
        if value is None:
            self._ref_image_set = False
        else:
            self._ref_image.value = np.asarray(value, dtype=np.float)
            self._ref_image_set = True

    def start_process_function(self):
        self._image_buffer = []
        self.clear_queues()
        self._previous_time = time.time()

        self.process_process = mp.Process(target=self._data_parsing_process)
        self.process_process.start()

    def stop_running(self):
        self.processes_active.value = False
        self._image_buffer = []

    def _data_parsing_process(self):
        while self.processes_active.value:
            time.sleep(self.process_sleep_time)
            if not self.receive_queue.empty():
                diffraction_image = self.receive_queue.get()
                self._image_buffer.append(diffraction_image)
                del diffraction_image

            self._time_now = time.time()

            time_delta = self._time_now - self._previous_time
            dt_bool = time_delta >= self.max_update_seconds.value
            nims = len(self._image_buffer)
            nim_bool = nims >= self.min_images_to_process.value
            if (nims > 0) and (dt_bool or nim_bool):
                self._previous_time = self._time_now

                # data should be (scanY, scanX, ..., detY, detX)
                # here, scanY is singular
                data = np.array(self._image_buffer)[None, ...]
                self._image_buffer = []
                if self._ref_image_set is False:
                    self._ref_image.value = data[0, 0]
                    self._ref_image_set = True
                shift_yx, _, _, _ = self._im_proc_func(
                        data, ref_im=self._ref_image.value.copy(),
                        **self.proc_func_args)
                # shift_yx is ((y,x), scanY, scanX, ...)
                shift_y, shift_x = shift_yx[:, 0, :]
                shift_r = (shift_y**2 + shift_x**2)**0.5
                shift_theta = np.arctan2(shift_y, shift_x)
                shift_theta_new = (shift_theta
                                   - np.deg2rad(self.component_angle.value))
                shift_component = shift_r * np.cos(shift_theta_new)
                shift_component -= self.base_level_correction.value
                shift_component /= self.fraction_correction.value

                for shift_component_i in shift_component:
                    self.output_queue.put(int(shift_component_i))
                del dt_bool, nims, nim_bool
                del data, shift_yx, shift_y, shift_x
                del shift_r, shift_theta, shift_theta_new
                del shift_component, shift_component_i


class CoMProcess(DataProcessingBase):
    """Process for getting a centre of mass component.

    Note: this class requires the fpd library.

    Attributes
    ----------
    aperture : None or 2-D bool array, optional
        Mask to apply to diffraction image. Note, True values are kept
        in the analysis. The `make_bf_mask` method can be used to generate
        the image.
    min_images_to_process : int, optional
        The minimum number of images to acquire before processing them.
    max_update_seconds : float, optional
        Maximum time in which to acquire images before processing.
    component_angle : float, optional
        Angle of the DPC signal to be returned, in degrees. The default
        is the x-component.
    proc_func_args : dictionary
        Center of mass function arguments. For details, see
        fpd.fpd_processing.center_of_mass.

    Notes
    -----
    The data is buffered and then multiple images are processed at once
    for efficiency. The calculations are triggered by the first of
    `min_images_to_process` or `max_update_seconds` to be exceeded.

    Examples
    --------
    >>> import fpd_live_imaging.testing_tools as tt
    >>> import fpd_live_imaging.acquisition_control_class as acc
    >>> scan_len = 32
    >>> test_detector = tt.TestDiskImages(
    ...     scan_len=scan_len,
    ...     radius=32, nscans=2, fast_but_big=True) # doctest: +SKIP
    >>> test_detector.sleep_time.value = 0.001 # doctest: +SKIP
    >>> test_detector.start_data_listening() # doctest: +SKIP
    >>> ac = acc.LiveImagingQt()
    >>> ac._comm_medi.port = test_detector.port # doctest: +SKIP
    >>> proc_name = ac.start_com_process() # doctest: +SKIP
    >>> rd = ac.process_dict[proc_name]['proc'] # doctest: +SKIP
    >>> rd.base_level_correction.value = -3 # doctest: +SKIP
    >>> rd.fraction_correction.value = 0.01 # doctest: +SKIP
    >>> rd.min_images_to_process.value = 256 # doctest: +SKIP
    >>> rd.aperture = rd.make_bf_mask(radius=40) # doctest: +SKIP
    >>> rd.proc_func_args['parallel'] = True # doctest: +SKIP
    >>> rd.proc_func_args['ncores'] = 4 # doctest: +SKIP
    >>> rd.max_update_seconds.value = 0.5 # doctest: +SKIP
    >>> rd.component_angle.value = 90.0 # doctest: +SKIP
    >>> rd.aperture = None # doctest: +SKIP
    >>> ac.resize_scan(*(scan_len,)*2)
    >>> ac.start_medipix_receiver()

    After the images have finished

    >>> ac.cancel_all_processes()
    >>> test_detector.stop_send_process() # doctest: +SKIP

    """

    def __init__(self, *args, **kwds):
        super(CoMProcess, self).__init__(
                *args, **kwds)

        try:
            from fpd.fpd_processing import center_of_mass
            self._im_proc_func = center_of_mass
        except ImportError:
            raise Exception(
                    'center_of_mass module cannot be loaded from fpd.')

        self.min_images_to_process = mp.Value('i', 128)  # 16 MB in 12bit mode
        self.max_update_seconds = mp.Value('f', 0.5)
        self.component_angle = mp.Value('f', 0.0)

        self._manager = mp.Manager()
        self.proc_func_args = self._manager.dict()
        self.proc_func_args.update({'nr': None,
                                    'nc': None,
                                    'aperture': None,
                                    'pre_func': None,
                                    'thr': 'otsu',
                                    'rebin': 4,
                                    'parallel': False,
                                    'ncores': 1,
                                    'print_stats': False})
        self._image_buffer = []  # holds images before processing
        self._previous_time = None
        self._time_now = None

    def make_bf_mask(
            self, centerX=128, centerY=128,
            imageSizeX=256, imageSizeY=256, radius=20):
        '''Generate bright field mask.

        Parameters
        ----------
        centerX : scalar, optional
            x-axis center.
        centerY : scalar, optional
            y-axis center.
        imageSizeX : integer, optional
            Image width in pixels.
        imageSizeY : integer, optional
            Image height in pixels.
        radius : scalar, optional
            Mask radius in pixels.

        Returns
        -------
        circular_mask : NumPy 2D array

        '''
        return self._make_circular_mask(
            centerX, centerY, imageSizeX, imageSizeY, radius)

    @property
    def aperture(self):
        return self.proc_func_args['aperture']

    @aperture.setter
    def aperture(self, value):
        if value is not None:
            value = value.astype(np.float)
        self.proc_func_args['aperture'] = value

    def start_process_function(self):
        self._image_buffer = []
        self.clear_queues()
        self._previous_time = time.time()

        self.process_process = mp.Process(target=self._data_parsing_process)
        self.process_process.start()

    def stop_running(self):
        self.processes_active.value = False
        self._image_buffer = []

    def _data_parsing_process(self):
        while self.processes_active.value:
            time.sleep(self.process_sleep_time)
            if not self.receive_queue.empty():
                diffraction_image = self.receive_queue.get()
                self._image_buffer.append(diffraction_image)
                del diffraction_image

            self._time_now = time.time()

            time_delta = self._time_now - self._previous_time
            dt_bool = time_delta >= self.max_update_seconds.value
            nims = len(self._image_buffer)
            nim_bool = nims >= self.min_images_to_process.value
            if (nims > 0) and (dt_bool or nim_bool):
                self._previous_time = self._time_now

                # data should be (scanY, scanX, ..., detY, detX)
                # here, scanY is singular
                data = np.array(self._image_buffer)[None, ...]
                self._image_buffer = []

                shift_yx = self._im_proc_func(
                        data, **self.proc_func_args)
                # shift_yx is ((y,x), scanY, scanX, ...)
                shift_y, shift_x = shift_yx[:, 0, :]
                shift_r = (shift_y**2 + shift_x**2)**0.5
                shift_theta = np.arctan2(shift_y, shift_x)
                shift_theta_new = (shift_theta
                                   - np.deg2rad(self.component_angle.value))
                shift_component = shift_r * np.cos(shift_theta_new)
                shift_component -= self.base_level_correction.value
                shift_component /= self.fraction_correction.value

                for shift_component_i in shift_component:
                    self.output_queue.put(int(shift_component_i))
                del dt_bool, nims, nim_bool
                del data, shift_yx, shift_y, shift_x
                del shift_r, shift_theta, shift_theta_new
                del shift_component, shift_component_i
