import sys
from PyQt5.QtWidgets import (
        QMainWindow, QLabel, QApplication, QCheckBox,
        QPushButton, QVBoxLayout, QHBoxLayout, QWidget, QSpinBox)
from PyQt5.QtGui import QPainter, QPen, QImage, QPixmap, QPalette
from PyQt5 import QtCore
import multiprocessing as mp


class ControlStarter(object):

    def __init__(self, icomm):
        self.icomm = icomm
        self._update_looper = True

    def start_ui_process(self):
        args = [self.icomm.control_parameters, self.icomm.scan_control]
        kwargs = ()
        self.ui_process = mp.Process(target=self._control_ui, args=args)
        self.ui_process.start()

    def _control_ui(self, control_parameters, scan_control):
        app = QApplication(sys.argv)
        dataview = ControlWindow(control_parameters, scan_control)
        dataview.show()
        sys.exit(app.exec_())

    def stop_running(self):
        self.ui_process.terminate()


class ControlWindow(QMainWindow):

    def __init__(self, control_parameters, scan_control):
        super().__init__()
        self.setWindowTitle("fpd_live_imaging control")
        self.control_parameters = control_parameters
        self.scan_control = scan_control
        self.initUI()

    def initUI(self):
        self.vbox = QVBoxLayout()

        hbox_scan = self.get_live_scan_hbox_buttons()
        hbox_scan_size = self.get_scan_size_hbox_buttons()
        hbox_scan_detectors = self.get_scan_detector_hbox_buttons()
        hbox_para_detectors = self.get_para_detector_hbox_buttons()
        hbox_hardware = self.get_hardware_hbox_buttons()
        hbox_utility = self.get_utility_hbox_buttons()

        self.vbox.addLayout(hbox_scan)
        self.vbox.addLayout(hbox_scan_size)
        self.vbox.addLayout(hbox_scan_detectors)
        self.vbox.addLayout(hbox_para_detectors)
        self.vbox.addLayout(hbox_hardware)
        self.vbox.addLayout(hbox_utility)

        self.widget0 = QWidget()
        self.widget0.setLayout(self.vbox)
        self.setCentralWidget(self.widget0)

    def close_window(self):
        self.control_parameters.__setitem__('stop_running', True)
        self.close()

    def get_live_scan_hbox_buttons(self):
        cpara = self.control_parameters
        hbox = QHBoxLayout()

        button_pm = QPushButton('-1 pixel')
        button_pm.clicked.connect(
                lambda: cpara.__setitem__('minus_scan_pos', True))
        hbox.addWidget(button_pm)

        button_pp = QPushButton('+1 pixel')
        button_pp.clicked.connect(
                lambda: cpara.__setitem__('plus_scan_pos', True))
        hbox.addWidget(button_pp)

        button_rs = QPushButton('Reset scan')
        button_rs.clicked.connect(
                lambda: cpara.__setitem__('restart_scan', True))
        hbox.addWidget(button_rs)

        return hbox

    def get_scan_size_hbox_buttons(self):
        cpara = self.control_parameters
        scan_control = self.scan_control
        hbox = QHBoxLayout()

        scan_x_input = QSpinBox(
                minimum=1., maximum=1025, keyboardTracking=False)
        scan_x_input.valueChanged.connect(
                lambda x: scan_control.__setitem__('x', x))
        hbox.addWidget(scan_x_input)

        scan_y_input = QSpinBox(
                minimum=1., maximum=1025, keyboardTracking=False)
        scan_y_input.valueChanged.connect(
                lambda y: scan_control.__setitem__('y', y))
        hbox.addWidget(scan_y_input)

        button_set_scan = QPushButton('Set scan size')
        button_set_scan.clicked.connect(
                lambda: cpara.__setitem__('set_scan_size', True))
        hbox.addWidget(button_set_scan)

        return hbox

    def get_scan_detector_hbox_buttons(self):
        cpara = self.control_parameters
        hbox = QHBoxLayout()

        button_bf = QPushButton('BF')
        button_bf.clicked.connect(
                lambda: cpara.__setitem__('add_bf', True))
        hbox.addWidget(button_bf)

        button_adf = QPushButton('ADF')
        button_adf.clicked.connect(
                lambda: cpara.__setitem__('add_adf', True))
        hbox.addWidget(button_adf)

        button_comxt = QPushButton('CoMxT')
        button_comxt.clicked.connect(
                lambda: cpara.__setitem__('add_comxt', True))
        hbox.addWidget(button_comxt)

        button_comyt = QPushButton('CoMyT')
        button_comyt.clicked.connect(
                lambda: cpara.__setitem__('add_comyt', True))
        hbox.addWidget(button_comyt)
        return hbox

    def get_para_detector_hbox_buttons(self):
        cpara = self.control_parameters
        hbox = QHBoxLayout()

        button_diff = QPushButton('Raw')
        button_diff.clicked.connect(
                lambda: cpara.__setitem__('add_diffraction', True))
        hbox.addWidget(button_diff)

        button_diff_f = QPushButton('FFT')
        button_diff_f.clicked.connect(
                lambda: cpara.__setitem__('add_diffraction_fft', True))
        hbox.addWidget(button_diff_f)

        button_diff_t = QPushButton('Threshold')
        button_diff_t.clicked.connect(
                lambda: cpara.__setitem__(
                    'add_diffraction_threshold', True))
        hbox.addWidget(button_diff_t)
        return hbox

    def get_hardware_hbox_buttons(self):
        cpara = self.control_parameters
        hbox = QHBoxLayout()

        button_ex = QPushButton('Insert detector')
        button_ex.clicked.connect(
                lambda: cpara.__setitem__('insert_detector', True))
        hbox.addWidget(button_ex)

        button_ex = QPushButton('Retract detector')
        button_ex.clicked.connect(
                lambda: cpara.__setitem__('retract_detector', True))
        hbox.addWidget(button_ex)
        return hbox

    def get_utility_hbox_buttons(self):
        cpara = self.control_parameters
        hbox = QHBoxLayout()

        button_ex = QPushButton('Start Medipix receiver')
        button_ex.clicked.connect(
                lambda: cpara.__setitem__(
                    'start_medipix_receiver', True))
        hbox.addWidget(button_ex)

        button_ex = QPushButton('Exit')
        button_ex.clicked.connect(self.close_window)
        hbox.addWidget(button_ex)
        return hbox
