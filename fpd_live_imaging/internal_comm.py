import time
import multiprocessing as mp
from multiprocessing.managers import SyncManager


class MyManager(SyncManager): pass


def Manager():
    m = MyManager()
    m.start()
    return m


class VariableControl(object):

    def __init__(self):
        pass


MyManager.register('VariableControl', VariableControl)


def add_scan_control(manager, variable_control):
    variable_control.scan_control = manager.dict()
    variable_control.scan_control['x'] = 64
    variable_control.scan_control['y'] = 64


def add_medipix_recv(manager, variable_control):
    variable_control.medipix_recv = manager.dict()
    variable_control.medipix_recv_bit_mode = manager.Value('i', 12)
    variable_control.medipix_recv_looper = manager.Value('b', True)
    variable_control.medipix_recv_raw_queue = manager.Queue()


def add_data_process(manager, variable_control, queue, name):
    queue_name = name + "_" + queue
    variable_control.__setattr__(queue_name, queue)

def add_data_process_utils(manager, variable_control):
    variable_control.data_process_queue_names = manager.list()


def add_control_parameters(manager, variable_control):
    variable_control.control_parameters = manager.dict({
            'minus_scan_pos': False,
            'plus_scan_pos': False,
            'restart_scan': False,
            'set_scan_size': False,
            'add_bf': False,
            'add_adf': False,
            'add_comxt': False,
            'add_comyt': False,
            'add_diffraction': False,
            'add_diffraction_fft': False,
            'add_diffraction_threshold': False,
            'insert_detector': False,
            'retract_detector': False,
            'stop_running': False,
            'start_medipix_receiver': False,
            })


def add_utility(manager, variable_control):
    variable_control.looper = manager.Value('b', True)


def start_internal_comm():
    manager = Manager()
    variable_control = manager.VariableControl()
    add_scan_control(manager, variable_control)
    add_medipix_recv(manager, variable_control)
    add_data_process_utils(manager, variable_control)
    add_control_parameters(manager, variable_control)
    add_utility(manager, variable_control)
    return variable_control, manager
