.. _api:

=================
API documentation
=================

Processing classes
==================
.. autoclass:: fpd_live_imaging.process_classes.DataProcessingBase
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.FullDiffractionImageProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.DiffractionImageRollingProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.FullDiffractionThresholdedImageProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.BFDetectorProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.ADFDetectorProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.SegmentedProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.SinglePixelProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.CoMxProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.CoMyProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.CoMxThresholdProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.CoMyThresholdProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.CoMxMaskDiskProcess
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.process_classes.CoMyMaskDiskProcess
    :members:
    :undoc-members:


Data reading
============

.. autoclass:: fpd_live_imaging.receive_data_medipix_class.ReceiveDataMedipix
    :members:
    :undoc-members:


Acquisition control
===================

.. autoclass:: fpd_live_imaging.acquisition_control_class.AcquisitionControlBase
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.acquisition_control_class.LiveImagingDM
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.acquisition_control_class.LiveImagingQt
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.acquisition_control_class.DummyAcquisition
    :members:
    :undoc-members:


Visualization
=============

.. autoclass:: fpd_live_imaging.live_qt_visualization.DataView
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.live_qt_visualization.DataViewParallel
    :members:
    :undoc-members:

.. autoclass:: fpd_live_imaging.live_qt_visualization.DataViewScanning
    :members:
    :undoc-members:

