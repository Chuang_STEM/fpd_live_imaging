.. _related_projects:

================
Related projects
================

- `merlin_interface <https://fast_pixelated_detectors.gitlab.io/merlin_interface/>`_: Python library for interfacing with a Medipix3 detector through the Merlin software
- `pixStem <http://pixstem.org>`_: post processing of pixelated STEM data.
