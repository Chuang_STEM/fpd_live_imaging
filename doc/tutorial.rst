.. _tutorial:

========
Tutorial
========

Open a IPython Qt terminal:

- For Windows using Anaconda: start-menu - Anaconda3 - *IPython* or *Jupyter QtConsole*.
- In Linux open an IPython terminal. For example by opening a terminal and running *ipython3*

This can also be done using the Jupyter Notebook: start-menu - Anaconda3 - *Jupyter Notebook*.

The code below can (mostly) be copied and pasted directly into either a QtConsole or a Notebook.


Using the control UI
====================

Testing locally
---------------

To see a test image, run (copy paste the whole code block into the IPython terminal):

.. code-block:: python


    >>> import fpd_live_imaging.testing_tools as tt
    >>> import fpd_live_imaging.acquisition_control_class as acc
    >>> from fpd_live_imaging.test_images.test_images import linux_penguin_64
    >>> acquisition_control = acc.LiveImagingQt()
    >>> test_detector = tt.TestDetectorInputImage(number_of_frames=10*64*64, input_image=linux_penguin_64)
    >>> test_detector.sleep_time.value = 0.01
    >>> test_detector.start_data_listening()
    >>> acquisition_control._comm_medi.port = test_detector.port
    >>> acquisition_control.start_control_update()
    >>> acquisition_control.start_ui_control()

In the Control UI window:

- Press *BF*
- Write 64 and 64 in the scan size boxes, then press *Set scan size*.
- Then press *Start Medipix receiver*

Note that the image viewer will change size.

When you want to stop, press the *Exit* button.
The script can also be stopped by writing in the console:

.. code-block:: python

    >>> acquisition_control.shutdown_everything()
    >>> test_detector.stop_send_process()


Live system
-----------

Running on a system with a Medipix3 detector and Merlin readout system.
This library interfaces with the Merlin readout system through the TCP/IP API.

First start the Merlin readout system, then open the Anaconda Jupyter QtConsole.

To start a simple live imaging (simply copy-paste into the QtConsole):

.. code-block:: python

    >>> import fpd_live_imaging.acquisition_control_class as acc
    >>> acquisition_control = acc.LiveImagingQt()
    >>> acquisition_control.start_control_update()
    >>> acquisition_control.start_ui_control()


Alternatively, to reset the live imaging, close the QtConsole.

Annular dark field:

.. code-block:: python

    >>> acquisition_control.start_adf_process()
    'ADF0'
    >>> adf0 = acquisition_control.process_dict['ADF0']['proc']
    >>> adf0.centreX = 100
    >>> adf0.centreY = 90
    >>> adf0.radius0 = 20
    >>> adf0.radius1 = 40

Magnetic imaging:

.. code-block:: python

    >>> acquisition_control.start_com_x_threshold_process()
    'CoMxT'
    >>> acquisition_control.start_com_y_threshold_process()
    'CoMyT'
    >>> com_x = acquisition_control.process_dict['CoMxT']['comm']
    >>> com_y = acquisition_control.process_dict['CoMyT']['comm']
    >>> com_x.threshold_multiplier = 2.
    >>> com_y.threshold_multiplier = 2.

To stop the live imaging

.. code-block:: python

    >>> acquisition_control.shutdown_everything()

Or simply press *Exit* in the control UI.


Using command line interface
============================

Testing locally
---------------


To see a test image, run (copy paste the whole code block into the IPython terminal):

.. code-block:: python

    >>> import fpd_live_imaging.testing_tools as tt
    >>> import fpd_live_imaging.acquisition_control_class as acc
    >>> from fpd_live_imaging.test_images.test_images import linux_penguin_64
    >>> acquisition_control = acc.LiveImagingQt()
    >>> test_detector = tt.TestDetectorInputImage(number_of_frames=64*64, input_image=linux_penguin_64)
    >>> test_detector.sleep_time.value = 0.001
    >>> test_detector.start_data_listening()
    >>> acquisition_control._comm_medi.port = test_detector.port
    >>> acquisition_control.start_bf_process()
    'BF0'
    >>> acquisition_control.resize_scan(64, 64)
    >>> acquisition_control.start_medipix_receiver()

To shutdown the script after the image has finished:

.. code-block:: python

    >>> acquisition_control.shutdown_everything()
    >>> test_detector.stop_send_process()


Live system
-----------

Running on a system with a Medipix3 detector and Merlin readout system.
This library interfaces with the Merlin readout system through the TCP/IP API.

First start the Merlin readout system, then open the Anaconda Jupyter QtConsole.

To start a simple live imaging (simply copy-paste into the QtConsole):

.. code-block:: python

    >>> import fpd_live_imaging.acquisition_control_class as acc
    >>> acquisition_control = acc.LiveImagingQt()
    >>> acquisition_control.start_bf_process()
    'BF0'
    >>> acquisition_control.start_full_diffraction_image_process()
    'Diffraction'
    >>> acquisition_control.resize_scan(64, 64)
    >>> acquisition_control.start_medipix_receiver()

Alternatively, to reset the live imaging, close the QtConsole.

Annular dark field:

.. code-block:: python

    >>> acquisition_control.start_adf_process()
    'ADF0'
    >>> adf0 = acquisition_control.process_dict['ADF0']['proc']
    >>> adf0.centreX = 100
    >>> adf0.centreY = 90
    >>> adf0.radius0 = 20
    >>> adf0.radius1 = 40

Magnetic imaging:

.. code-block:: python

    >>> acquisition_control.start_com_x_threshold_process()
    'CoMxT'
    >>> acquisition_control.start_com_y_threshold_process()
    'CoMyT'
    >>> com_x = acquisition_control.process_dict['CoMxT']['comm']
    >>> com_y = acquisition_control.process_dict['CoMyT']['comm']
    >>> com_x.threshold_multiplier = 2.
    >>> com_y.threshold_multiplier = 2.

To stop the live imaging

.. code-block:: python

    >>> acquisition_control.shutdown_everything()
